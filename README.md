# tasker

Simple Todo application with distraction-free management mechanisms (that second part is still WIP).

Requires tasker_server (see other repo) to run.

## Project Setup

```sh
npm install
```

### Run Vite development server (currently no deployment)

```sh
npm run dev
```

