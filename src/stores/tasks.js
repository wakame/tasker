import { defineStore } from 'pinia'
import axios from 'axios'

const API_PREFIX = 'http://localhost:7999/api/v1/'
const TASKS_PATH = API_PREFIX + 'tasks'

function taskStatusOpen(task) {
  return task.status == 'Open'
}

function getOpenTasks(state) {
  return state.tasks.filter(taskStatusOpen)
}

function getCurrentTaskTitle(state) {
  if (state.currentTask) {
    return state.currentTask.title
  }
  return 'No current task'
}

export async function fetchTasks(store) {
  const response = await axios.get(TASKS_PATH)
  store.setTasks(response.data)
}

export async function putTasks(tasks) {
  axios.post(TASKS_PATH, tasks)
}

export const useTaskStore = defineStore('tasks', {
  state: () => {
    const taskStore = {
      tasks: [],
      currentTask: null
    }

    return taskStore
  },
  getters: {
    openTasks: (state) => getOpenTasks(state),
    currentTaskTitle: (state) => getCurrentTaskTitle(state)
  },
  actions: {
    addTask(task) {
      this.tasks.push(task)
      putTasks(this.tasks)
    },
    setTasks(tasks) {
      this.tasks = tasks
    }
  }
})
