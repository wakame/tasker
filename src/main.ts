import './assets/main.css'

import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import { createPinia } from 'pinia'
import App from './App.vue'

import Overview from './components/Overview.vue'
import AddTasks from './components/AddTasks.vue'
import EnrichTasks from './components/EnrichTasks.vue'
import DoTasks from './components/DoTasks.vue'
import DebugView from './components/DebugView.vue'

import { fetchTasks, useTaskStore } from './stores/tasks.js'

const routes = [
  { path: '/', component: Overview },
  { path: '/addTasks', component: AddTasks },
  { path: '/enrichTasks', component: EnrichTasks },
  { path: '/doTasks', component: DoTasks },
  { path: '/debugView', component: DebugView }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

const app = createApp(App)

app.use(router)

const pinia = createPinia()
app.use(pinia)

const taskStore = useTaskStore()
fetchTasks(taskStore)

app.mount('#app')
