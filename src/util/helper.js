export function getRandomIndex(list) {
  return Math.floor(Math.random() * list.length)
}

export function getRandomListElement(list) {
  if (list.length == 0) {
    throw new Error('Got empty list')
  }
  let i = getRandomIndex(list)
  return list[i]
}
